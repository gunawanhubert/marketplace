import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import Header from "../layout/Header";
import Footer from "../layout/Footer";
import HeaderBack from "../layout/HeaderBack";


class HistoryTransaction extends React.Component{
    render(){
        return(
            <React.StrictMode>
                <HeaderBack title="History" />
                {/* <h1>History</h1> */}
                <Footer />
            </React.StrictMode>
        );
    }
}

export default HistoryTransaction;
