import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import HeaderBack from "../layout/HeaderBack";


class Register extends React.Component{
    render(){
        return(
            <React.StrictMode>
                <HeaderBack title="Register" /> 
            </React.StrictMode>
        );
    }
}

export default Register;
