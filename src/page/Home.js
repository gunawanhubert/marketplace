import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import Header from "../layout/Header";
import Footer from "../layout/Footer";


class Home extends React.Component{
    render(){
        return(
            <React.StrictMode>
                <Header />
                <h1>Home</h1>
                <Footer />
            </React.StrictMode>
        );
    }
}

export default Home;
