import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import Header from "../layout/Header";
import Footer from "../layout/Footer";
import { Redirect } from "react-router";


class Cart extends React.Component{
    
    constructor(props){
        super(props);
        this.state ={
            isLogged : false
        }
    }

    render(){
        return(
            <React.StrictMode>
                <h1>Cart</h1>
                <Footer />
            </React.StrictMode>
        );
    }
}

export default Cart;
