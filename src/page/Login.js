import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import HeaderBack from "../layout/HeaderBack";
import { useLocalStorage } from "../useLocalStorage";
import Header from "../layout/Header";
import Footer from "../layout/Footer";
import { Link } from "react-router-dom";


// class Login extends React.Component{
//     constructor(props){
//         super(props);

//     }
    
//     render(){
        
//     }
// }

const Login = ()=>{
    const [token, setToken] = useLocalStorage("token", "");
    const [username, setUsername] = useLocalStorage("username", "");
    const [password, setPassword] = useLocalStorage("password", "");

    async function LoginUser(){
        return fetch(`${API_URL}auth/login`,{
            method: "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body:{
                username: username,
                password: password
            }
        }).then(data => data.json())
    }

    async function SubmitHandler(event){
        event.preventDefault();
        let response = await LoginUser();
        if(response.status == 200){
            setToken(response.token);
            window.location = '/';
        }else{
            
        }
    }

    return(
        <React.StrictMode>
            <HeaderBack title="Login"/>
            <div className="row">
                <form className="col s12">
                    <div className="row">
                        <div className="input-field col s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="Username" type="text" className="validate" onChange={ e => setUsername(e.target.value)}/>
                            <label for="Username">Username</label>
                        </div>
                        <div className="input-field col s12">
                            <i class="material-icons prefix">lock</i>
                            <input id="Password" type="password" className="validate" onChange={ e => setPassword(e.target.value)}/>
                            <label for="Password">Password</label>
                        </div>
                        {/* <div class="col s2"></div> */}
                        <div class="col s12 center">
                            <a className={`btn ${THEME_COLOR}`} onClick={SubmitHandler}><i></i>Login</a>
                        </div>
                        {/* <div class="col s2"></div> */}
                        
                    </div>
                    {/* <hr /> */}
                    <div className="row center">
                        don't have account? <Link to="/signup">sign up</Link>
                    </div>
                </form>
            </div>
        </React.StrictMode>
    );
}

export default Login;
