import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import Header from "../layout/Header";
import Footer from "../layout/Footer";
import { useLocalStorage } from "../useLocalStorage";
import { Redirect } from "react-router";

// class Profile extends React.Component{
//     render(){
//     }
// }

const Profile = ()=>{
    const [isLogged, setIsLoged] = useLocalStorage("isLoged", "");
    
    function LogoutHandler(event){
        setIsLoged(false);
        window.location = '/';
    }

    return(
        <React.StrictMode>
            <h1>Profile</h1>
            <a className={`btn`} onClick={LogoutHandler}>Logout</a>
            <Footer />
        </React.StrictMode>
    );
}

export default Profile;
