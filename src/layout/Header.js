// import logo from './logo.svg';
import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';


class Header extends React.Component{
    render(){
        return(
            <nav className={`${THEME_COLOR} hide-on-med-and-down`}>
                <div className="nav-wrapper">
                    <a href="#" className="hide-on-med-and-down brand-logo">Miracle Houseware</a>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li><a href="sass.html">Sass</a></li>
                        <li><a href="badges.html">Components</a></li>
                        <li><a href="collapsible.html">JavaScript</a></li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;
