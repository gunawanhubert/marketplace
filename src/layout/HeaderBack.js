import './HeaderBack.css';
import {API_URL, THEME_COLOR} from "../constant"
import React from 'react';
import { Link, useHistory } from "react-router-dom";


// class HeaderBack extends React.Component{
//     constructor(props){
//         super(props);
//         this.state ={
//             title: this.props.title
//         }
//     }

//     render(){
//     }
// }

const HeaderBack = (props) => {
    let history = useHistory();

    function GoBackHandler(){
        history.goBack();
    }

    return(
        <div className="header-bar">
            <a className={`btn-flat white`} onClick={GoBackHandler}  style={{fontSize: "2rem"}}><i className="material-icons left"  style={{fontSize: "2rem"}}>arrow_back</i>{props.title}</a>
        </div>
    );
}

export default HeaderBack;
