import './Footer.css';
import {THEME_COLOR} from "../constant"
import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';


class Footer extends React.Component{
    render(){
        return(
            <footer className={`${THEME_COLOR} page-footer hide-on-large-only`}>
                <div className={`row`} id="frow" style={{marginBottom: "0px"}}>
                    <div className="col s12" style={{paddingLeft:"0px !important", paddingRight:"0px !important"}}>
                        <ul className="tabs tabs-fixed-width transparent white-text">
                            <li className="tab col s3">
                                <Link to="/" className="white-text">
                                    <i className="material-icons">home</i>
                                </Link>
                            </li>
                            <li className="tab col s3">
                                <Link to="/" className="white-text">
                                    <i className="material-icons">search</i>
                                </Link>
                            </li>
                            <li className="tab col s3">
                                <Link to="/cart" className="white-text">
                                    <i className="material-icons">local_grocery_store</i>
                                </Link>
                            </li>
                            <li className="tab col s3">
                                <Link to="/history" className="white-text">
                                    <i className="material-icons">assignment</i>
                                </Link>
                            </li>
                            <li className="tab col s3">
                                <Link to="/profile" className="white-text">
                                    <i className="material-icons">account_circle</i>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
