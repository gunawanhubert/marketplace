import logo from './logo.svg';
import './App.css';
import {API_URL} from "./constant";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import allReducer from './reducer';
import { useLocalStorage } from './useLocalStorage';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Redirect } from 'react-router';
import Home from "./page/Home";
import Cart from "./page/Cart";
import HistoryTransaction from "./page/HistoryTransaction";
import Profile from "./page/Profile";
import Login from './page/Login';
import Register from './page/Register';


let store = createStore(allReducer);

function App() {
  
  const [token, setToken] = useLocalStorage("token", "");
  // let isLogged = false;
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/cart" exact>
            {token ? <Cart/> : <Redirect to="/login"/>}
          </Route>
          <Route path="/history" exact>
            {token ? <HistoryTransaction /> : <Redirect to="/login"/>}
          </Route>
          <Route path="/Profile" exact>
            {token ? <Profile/> : <Redirect to="/login"/>}
          </Route>
          <Route path="/login" exact>
            {!token ? <Login/> : <Redirect to="/"/>}
          </Route>
          <Route path="/signup" exact>
            {!token ? <Register/> : <Redirect to="/"/>}
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
